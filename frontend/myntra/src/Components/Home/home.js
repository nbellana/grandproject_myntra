import React, { useState } from "react";
import Navbar from "../Navbar/navbar";
import { Footer } from "../Footer/footer";

import "../css/home.css";
import "../css/util.css";

const HomeComponent = () => {
  return (
    <>
      <Navbar />
      <section className="container sec1">
        <div className="mySlides fade">
          <img
            className="main-img"
            alt="img"
            src="https://assets.myntassets.com/w_980,c_limit,fl_progressive,dpr_2.0/assets/images/2022/5/31/4031994d-9092-4aa7-aea1-f52f2ae5194f1654006594976-Activewear_DK.jpg"
            style={{ width: "100%" }}
          />
        </div>
        <div className="mySlides fade">
          <img
            className="main-img"
            alt="img"
            src="https://assets.myntassets.com/w_980,c_limit,fl_progressive,dpr_2.0/assets/images/2022/6/27/1b39ae61-c690-4b92-b3ec-21f337ca9e851656324904938-Roadster_Desk_Banner.jpg"
            style={{ width: "100%" }}
          />
        </div>
        <div className="mySlides fade">
          <img
            className="main-img"
            alt="img"
            src="https://assets.myntassets.com/w_980,c_limit,fl_progressive,dpr_2.0/assets/images/2022/6/1/9041bdba-b48a-4ed9-8fcb-e1eeb23a74e31654099110315-Sports---Casual-Shoes_Desk.jpg"
            style={{ width: "100%" }}
          />
        </div>
        <div className="mySlides fade">
          <img
            className="main-img"
            alt="img"
            src="https://assets.myntassets.com/w_980,c_limit,fl_progressive,dpr_2.0/assets/images/2022/6/27/53b4daed-cd2c-4111-86c5-14f737eceb351656325318973-Handbags_Desk.jpg"
            style={{ width: "100%" }}
          />
        </div>

        <h4 className="sec-text">BEST OF INTERNATIONAL BRANDS</h4>
        <img
          className="main-img"
          alt="img"
          src="https://assets.myntassets.com/w_980,c_limit,fl_progressive,dpr_2.0/assets/images/2022/6/27/df7e71d9-c221-46b3-8dee-6f116a6b90ab1656352346755-Desktop-Banner-.gif"
        />

        <h4 className="bet-text">TOP PICKS</h4>
        <section className="container1 sec1 flex-wrap">
          <img
            className="sub-img"
            alt="img"
            src="https://assets.myntassets.com/w_140,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/062cea23-9a6a-44b9-bdd4-87cae6a462311645602543339-Kurta-sets.jpg"
          />
          <img
            className="sub-img"
            alt="img"
            src="https://assets.myntassets.com/w_140,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/8d65d400-decd-4f42-902c-a40350a16ed11645602543346-Kurtas.jpg"
          />
          <img
            className="sub-img"
            alt="img"
            src="https://assets.myntassets.com/w_140,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/398ee53b-5899-4a9a-9d0b-b35d60c01cb41645602543325-Dresses.jpg"
          />
          <img
            className="sub-img"
            alt="img"
            src="https://assets.myntassets.com/w_140,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/2f410d81-7fae-400e-9ecc-b4a8b6df72b91645602543430-Women-Jeans.jpg"
          />
          <img
            className="sub-img"
            alt="img"
            src="https://assets.myntassets.com/w_140,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/c6b09c0f-5c57-472c-a3fc-854ec506a90e1645602543387-Men-T-shirt.jpg"
          />
        </section>

        <h4 className="bet-text">CATEGORIES TO BAG</h4>
        <section className="container1 sec1 flex-wrap">
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/aa4658b5-d723-4652-9ea1-00456b355c3a1645602467046-Kurta-Sets.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/aae4be67-e611-47f4-b94e-92a16a36df731645602467007-Hangbags.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/0f0be09e-4155-47bf-82e1-51044e7e7fd11645602467052-Kurtas.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/dd4414f8-4e1b-4a22-997e-8e06c0a5ff861645602467167-T-Shirts.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/f9ca5609-b634-42d4-8c08-a8eaebb818b71645602467085-Sarees.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/0b7869d4-f825-4625-b1db-58ad10a45f301645602467093-Shirts.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/b34a30a6-504b-4c94-b7e1-61391d536bc51645602467038-Jewellery.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/b156f76a-26e7-4bce-9941-8a67d3c16f331645602467120-Teens-Wear.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/fb091b07-c275-4578-b08d-b4f93dfe9e841645602466976-Beauty.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/b827f900-ed61-4467-84fa-a6e357787e761645602467079-Plus-Size-Styles_W.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/cd083042-3bb2-4231-8b96-0234fc0ed23f1645602467032-Jeans.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/736f3951-e67b-414f-bfb1-56e2794d441d1645602467114-Sports-Shoes.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/d13255df-c846-4dbd-8458-77ccaba4f9eb1645602467142-Trousers.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/7a774194-94e6-49b5-b8bb-64bf9901bc671645602466989-Casual-Shoes.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/5860c3c2-a639-4625-ac1d-4d55406f128a1645602467134-Track-Pants.jpg"
          />
          <img
            className="sub-img1"
            alt="img"
            src="https://assets.myntassets.com/w_122,c_limit,fl_progressive,dpr_2.0/assets/images/2022/2/23/07e5c29a-2eab-4b2d-b617-6565ffe1f4701645602467025-Innerwear.jpg"
          />
        </section>

        <h4 className="bet-text">GIFTING CARDS</h4>
        <section className="container1 sec1 flex-wrap">
          <img
            className="sub-img2"
            alt="img"
            src="https://assets.myntassets.com/w_196,c_limit,fl_progressive,dpr_2.0/assets/images/2021/11/12/ef1e7c6b-bec4-471c-9c36-72fffd975c0c1636738225251-happy-wedding.jpg"
          />
          <img
            className="sub-img2"
            alt="img"
            src="https://assets.myntassets.com/w_196,c_limit,fl_progressive,dpr_2.0/assets/images/2021/11/12/72f3fe20-f0a5-4894-852c-817b40b285291636738225244-happy-birthday.jpg"
          />
          <img
            className="sub-img2"
            alt="img"
            src="https://assets.myntassets.com/w_196,c_limit,fl_progressive,dpr_2.0/assets/images/2021/11/12/0b639a7e-20ed-4c37-87b4-279616accc2b1636738225231-house-warming.jpg"
          />
          <img
            className="sub-img2"
            alt="img"
            src="https://assets.myntassets.com/w_196,c_limit,fl_progressive,dpr_2.0/assets/images/2021/11/12/37fe74de-0cb1-4aec-adc1-41c9de64bc431636738225237-farewell.jpg"
          />
          <img
            className="sub-img2"
            alt="img"
            src="https://assets.myntassets.com/w_196,c_limit,fl_progressive,dpr_2.0/assets/images/2021/11/12/934a734c-ebb4-48c4-96d0-018e1e5eb7e21636738225215-milestones.jpg"
          />
        </section>

        <h4 className="sec-text">MEN</h4>
        <img
          className="main-img"
          alt="img"
          src="https://assets.myntassets.com/w_980,c_limit,fl_progressive,dpr_2.0/assets/images/2022/5/3/e384cb32-690c-4ccf-a6cb-61df36960bb21651599573972-Workwear_Desk.jpg"
        />

        <h4 className="sec-text">WOMEN</h4>
        <img
          className="main-img"
          alt="img"
          src="https://assets.myntassets.com/w_980,c_limit,fl_progressive,dpr_2.0/assets/images/2022/5/3/3f6d1e2a-5ef6-4921-be5d-443a11b11d801651599573985-Dresses_Desk.jpg"
        />

        <h4 className="sec-text">KIDS</h4>
        <img
          className="main-img"
          alt="img"
          src="https://assets.myntassets.com/w_980,c_limit,fl_progressive,dpr_2.0/assets/images/2022/4/12/09f0df54-6f8f-4bb0-a4b9-3b374d4538561649782019495-Top-Brands-2_Desk_Banner.jpg"
        />

        <h4 className="sec-text">HOME & LIVING</h4>
        <img
          className="main-img"
          alt="img"
          src="https://assets.myntassets.com/w_980,c_limit,fl_progressive,dpr_2.0/assets/images/banners/2018/6/8/eff01060-f706-468d-b97c-95cdf43174f91528443826867-Desktop-Home-Banner.jpg"
        />
      </section>

      <Footer />
    </>
  );
};

export default HomeComponent;
