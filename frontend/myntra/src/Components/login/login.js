import React, { useState } from "react";

import Navbar from "../Navbar/navbar";
import { Footer } from "../Footer/footer";

import img from "../../images/login.webp";
import "./login.css";

const Login = () => {
  const [mobileno, setmobileno] = useState("");
  let par = document.getElementById("error");

  const continues = () => {
    if (mobileno.length > 10) {
      par.innerHTML = "Error: Pls note you enter wrong mobile no";
    }
    if (mobileno.length === 10) {
      par.innerHTML = "";
      const myForm = {
        phonenumber: mobileno,
      };
    }
    if (mobileno.length === 0) {
      par.innerHTML = "Error: Pls enter mobile no";
    }
    if (mobileno.length < 10 && mobileno.length > 0) {
      par.innerHTML = "Error: Pls note you enter wrong mobile no";
    }
  };

  return (
    <>
      <Navbar />
      <div className="login_container1">
        <div className="login_container2">
          <img className="login_img" src={img} alt="img" srcset="" />
          <div className="matter">
            <h2 className="login_h2">Login</h2>
            <input
              className="login_input"
              type="text"
              placeholder="+91 | Mobile Number"
              onChange={(e) => setmobileno(e.target.value)}
              required
            />
            {/* <p id="error"></p> */}
            <input
              className="login_input"
              type="password"
              placeholder="Password"
              required
            />
            <p className="login_p">
              By continuing, I agree to the{" "}
              <b className="login_b">Terms of Use</b> &{" "}
              <b className="login_b">Privacy Policy</b>
            </p>
            <button className="login_button" type="submit" onClick={continues}>
              CONTINUE
            </button>
            <p className="login_p">
              Have trouble logging in? <b className="login_b">Get help</b>{" "}
            </p>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Login;
