//External imports
import React from "react";

//Internal imports
import Navbar from "../Navbar/navbar";
import { Footer } from "../Footer/footer";

import "../login/login.css";

class Register extends React.Component {
  // const [username, setUsername] = useState("");
  // const [email, setEmail] = useState("");
  // const [password, setPassword] = useState("");
  // const [number, setNumber] = useState("");
  // const [gender, setGender] = useState("");

  // const onChangeHandler = (ev) => {
  //   if (ev.target.name === "username") {
  //     setUsername(ev.target.value);
  //   } else if (ev.target.name === "password") {
  //     setPassword(ev.target.value);
  //   } else if (ev.target.name === "email") {
  //     setEmail(ev.target.value);
  //   } else if (ev.target.name === "number") {
  //     setNumber(ev.target.value);
  //   } else if (ev.target.name === "gender") {
  //     setGender(ev.target.value);
  //   } else {
  //     console.log(ev.target.name + " did not match with anyone");
  //   }
  // };

  // const onClickHandler = (ev) => {
  //   // check if username, password, gender, email, number are all valid
  //   let isFormValid = true;
  //   // do validation -----

  //   console.log({ email, password, username, gender, number });

  //   if (isFormValid) {
  //     const requestOptions = {
  //       method: "POST",
  //       headers: { "Content-Type": "application/json" },
  //       body: JSON.stringify({ email, password, gender, number, username }),
  //     };
  //     fetch("http://localhost:8000/create_user", requestOptions)
  //       .then((response) => response.json())
  //       .then((data) => console.log(data));
  //   }
  // };

  constructor(props) {
    super(props);

    this.state = {
      username: "",
      // username_err: "",
      email: "",
      password: "",
      phone: "",
    };

    this.onInputChanged = this.onInputChanged.bind(this);
    this.onButtonClick = this.onButtonClick.bind(this);
  }

  onInputChanged(et) {
    console.log(et.target.name, et.target.value);
    if (et.target.name === "username") {
      this.setState((prevState) => ({
        username: et.target.value,
      }));
    } else if (et.target.name === "password") {
      this.setState((prevState) => ({
        password: et.target.value,
      }));
    } else if (et.target.name === "email") {
      this.setState((prevState) => ({
        email: et.target.value,
      }));
    } else if (et.target.name === "phone") {
      this.setState((prevState) => ({
        phone: et.target.value,
      }));
    } else {
      console.log("unknown target name, dont know how to respond.");
    }
  }

  onButtonClick(et) {
    // validate it and do the below ONLY if validation passes

    const data = {
      username: this.state.username,
      password: this.state.password,
      email: this.state.email,
      phone: this.state.phone,
    };

    fetch("http://localhost:8000/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Success:", data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }
  render() {
    return (
      <>
        <Navbar />
        <div className="login_container1">
          <div className="login_container2">
            <img
              className="login_img"
              src="//assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/2022/8/16/cee6eeec-75c4-46b6-a84e-6467d0fb020c1660588808179-offer-banner-300-600x240-code-_-MYNTRA200.jpg"
              alt="img"
            />
            <div className="matter">
              <h2 className="login_h2">Register</h2>
              <input
                className="login_input"
                name="phone"
                type="number"
                placeholder="+91 | Mobile Number"
                value={this.state.phone}
                onChange={this.onInputChanged}
                required
              />
              {/* <p id="error" className="text-xs text-red-500 "></p> */}
              <input
                className="login_input"
                name="email"
                type="text"
                placeholder="Email"
                value={this.state.email}
                onChange={this.onInputChanged}
                required
              />
              <input
                className="login_input"
                name="username"
                type="text"
                placeholder="Username"
                value={this.state.username}
                onChange={this.onInputChanged}
                required
              />
              <input
                className="login_input"
                type="password"
                name="password"
                placeholder="Password"
                value={this.state.password}
                onChange={this.onInputChanged}
                required
              />

              <p className="login_p">
                By continuing, I agree to the{" "}
                <b className="login_b">Terms of Use</b> &{" "}
                <b className="login_b">Privacy Policy</b>
              </p>
              <button
                className="login_button"
                type="submit"
                onClick={this.onButtonClick}
              >
                SIGNUP
              </button>
              <p className="login_p">
                Have trouble logging in? <b className="login_b">Get help</b>{" "}
              </p>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

export default Register;
