import React from "react";
import "../css/home.css";
import "../css/util.css";

const navbar = () => {
  return (
    <>
      <header className="container">
        <nav className="flex space-between">
          <div className="left flex items-center">
            <img
              src="https://upload.wikimedia.org/wikipedia/commons/b/bc/Myntra_Logo.png"
              alt="img"
            />
            <ul className="flex items-center justify-center semibold">
              <li id="C1">MEN</li>
              <li id="C2">WOMEN</li>
              <li id="C3">KIDS</li>
              <li id="C4">HOME & LIVING</li>
              <li>BEAUTY</li>
              <li>STUDIO</li>
            </ul>
          </div>

          <div className="right flex items-center">
            <input
              className="searchbar desktop-searchBar"
              placeholder="Search for products, brands and more"
              data-reactid="904"
            />
            <div className="profile mx-2">Profile</div>
            <div className="wishlist mx-2">Wishlist</div>
            <div className="bag mx-2">Bag</div>
          </div>
        </nav>
      </header>
    </>
  );
};

export default navbar;
