import LoginComponent from "./login/login";
import RegisterComponent from "./register/register";
import HomeComponent from "./Home/home";

export {
  LoginComponent as Login,
  RegisterComponent as Register,
  HomeComponent as Home,
};
